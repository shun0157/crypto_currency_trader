from time import sleep

import logger
from dto.best_arbitrage_dto import BestArbitrageDto
from enums.trading_floor_type import TradingFloorType
from utils.bitbank_utils import BitbankUtils
from utils.bitflyer_utils import BitflyerUtils
from utils.coincheck_utils import CoincheckUtils
from utils.liquid_utils import LiquidUtils

logger = logger.Logger.get_logger()
while True:
    market_price_dto_list = []
    try:
        market_price_dto_list.append(BitflyerUtils.get_btc_jpy_market_price())
        market_price_dto_list.append(LiquidUtils.get_btc_jpy_market_price())
        market_price_dto_list.append(CoincheckUtils.get_btc_jpy_market_price())
        market_price_dto_list.append(BitbankUtils.get_btc_jpy_market_price())
    except Exception as e:
        logger.error(e)
        continue

    for dto in market_price_dto_list:
        logger.info(dto)

    best_arbitrage_dto = BestArbitrageDto(
        asks_trading_floor_type=TradingFloorType.NONE,
        bids_trading_floor_type=TradingFloorType.NONE,
        asks=0,
        bids=0,
    )
    for asks_dto in market_price_dto_list:
        for bids_dto in market_price_dto_list:
            if asks_dto.trading_floor_type is bids_dto.trading_floor_type:
                continue
            difference_price = bids_dto.bids - asks_dto.asks
            if difference_price > best_arbitrage_dto.get_difference_price():
                best_arbitrage_dto.bids_trading_floor_type = bids_dto.trading_floor_type
                best_arbitrage_dto.asks_trading_floor_type = asks_dto.trading_floor_type
                best_arbitrage_dto.bids = bids_dto.bids
                best_arbitrage_dto.asks = asks_dto.asks

    logger.info("*****************result******************")
    logger.info(f"+{best_arbitrage_dto.get_difference_price()}円")
    logger.info(best_arbitrage_dto)
    logger.info("*****************************************")
    if best_arbitrage_dto.get_difference_price() > 5000:
        logger.info("!!!!!!!!!!!!!big chance!!!!!!!!!!!!!!")
    sleep(5)
# 1. 各取引所の現在の売値,買値,スプレッド,24時間の取引高を非同期で取得

# 2. 全ての取引所で取得できたら一番利益がでる組み合わせをみつけて非同期で同時に注文を入れる

# 3. どちらか一方が失敗した場合現状一番価格差がある取引所を再取得後注文をいれる(もっかい行く必要ない？)

# 4. 結果を出力してDBに結果を書き込む

# 5. 現在のtotal balanceを出力する

# 成り行きでやる場合%ベースでやったほうがいい、ちっちゃな金額で試してみる
# ドルだてのほうが差は大きいことの方がおおい
# 複雑になるからやっている人が少ない
# 売る=ショート
# 買う=ロング
# 1BTC買いました300万
# 同時に1BTCを売りボジションで持つ
# bitflyer & GMO
# 手数料%の何倍開いたときに注文いれるかを決めたほうがいい
