from logging import getLogger, Formatter, StreamHandler, FileHandler, DEBUG, INFO

from settings import settings


class Logger:
    logger = None

    @classmethod
    def get_logger(cls):
        if cls.logger:
            return cls.logger
        else:
            cls.logger = getLogger()
            cls.logger = cls._set_handler(logger=cls.logger, handler=StreamHandler(), level=settings.logging.level)
            cls.logger = cls._set_handler(logger=cls.logger, handler=FileHandler("logs/trade.log"), level=INFO)
            cls.logger.setLevel(DEBUG)
            cls.logger.propagate = False
            return cls.logger

    @staticmethod
    def _set_handler(logger, handler, level):
        handler.setLevel(level)
        handler.setFormatter(Formatter('%(asctime)-15s [%(levelname)s] %(message)s'))
        logger.addHandler(handler)
        return logger
