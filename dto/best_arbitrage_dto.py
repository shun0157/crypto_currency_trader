import dataclasses

from enums.trading_floor_type import TradingFloorType


@dataclasses.dataclass
class BestArbitrageDto:
    bids_trading_floor_type: TradingFloorType
    asks_trading_floor_type: TradingFloorType
    bids: float
    asks: float

    def get_difference_price(self):
        return self.bids - self.asks
