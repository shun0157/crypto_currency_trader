import dataclasses

from enums.trading_floor_type import TradingFloorType


@dataclasses.dataclass
class MarketPriceDto:
    trading_floor_type: TradingFloorType
    bids: float
    asks: float
