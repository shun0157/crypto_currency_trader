import logger
from dto.market_price_dto import MarketPriceDto
from enums.trading_floor_type import TradingFloorType
from utils import bitbank
from utils.coincheck import market

logger = logger.Logger.get_logger()

client = market.Market()


class BitbankUtils:
    client = bitbank.public()
    trading_floor_type = TradingFloorType.BIT_BANK

    def __init__(self):
        pass

    @classmethod
    def get_btc_jpy_market_price(cls):
        response = cls.client.get_ticker('btc_jpy')
        for k, v in response.items():
            logger.debug(f"{k}, {v}")
        market_price_dto = MarketPriceDto(
            trading_floor_type=cls.trading_floor_type,
            bids=float(response.get("sell")),
            asks=float(response.get("buy")),
        )
        return market_price_dto

    @classmethod
    def get_balance(cls):
        pass
