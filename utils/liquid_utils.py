from quoine.client import Quoinex

import logger
from dto.market_price_dto import MarketPriceDto
from enums.trading_floor_type import TradingFloorType
from settings import settings

logger = logger.Logger.get_logger()


class LiquidUtils:
    client = Quoinex(settings.liquid.api_key, settings.liquid.secret)
    trading_floor_type = TradingFloorType.LIQUID

    def __init__(self):
        pass

    @classmethod
    def get_btc_jpy_market_price(cls):
        response = cls.client.get_product(product_id=5)
        for k, v in response.items():
            logger.debug(f"{k}, {v}")
        market_price_dto = MarketPriceDto(
            trading_floor_type=cls.trading_floor_type,
            bids=float(response.get("market_bid")),
            asks=float(response.get("market_ask"))
        )
        return market_price_dto

    @classmethod
    def get_balance(cls):
        pass
