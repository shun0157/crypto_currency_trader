from bitflyer import public, private

import logger
from dto.market_price_dto import MarketPriceDto
from enums.trading_floor_type import TradingFloorType
from settings import settings

logger = logger.Logger.get_logger()


class BitflyerUtils:
    private_client = private.Private(access_key=settings.bit_flyer.api_key,
                                     secret_key=settings.bit_flyer.secret)
    public_client = public.Public()
    trading_floor_type = TradingFloorType.BIT_FLYER

    def __init__(self):
        pass

    @classmethod
    def get_btc_jpy_market_price(cls):
        response = cls.public_client.getticker()
        for k, v in response.items():
            logger.debug(f"{k}, {v}")
        market_price_dto = MarketPriceDto(
            trading_floor_type=cls.trading_floor_type,
            bids=float(response.get("best_bid")),
            asks=float(response.get("best_ask")),
        )
        return market_price_dto

    @classmethod
    def get_balance(cls):
        print(cls.private_client.getbalance())
