from enum import Enum


class TradingFloorType(Enum):
    NONE = "none"
    BIT_FLYER = "bit_flyer"
    LIQUID = "liquid"
    COIN_CHECK = "coin_check"
    BIT_BANK = "bit_bank"
